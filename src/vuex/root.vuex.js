import Vue from 'vue';
import Vuex from 'vuex';
import ServiceModule from './services.vuex';
import QuestionModule from './questions.vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    strict: true,
    modules: {
        services: ServiceModule,
        questions: QuestionModule
    }
});
