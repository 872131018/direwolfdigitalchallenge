const module = {
    state: {
        loading: 0
    },
    mutations: {
        SERVICE_LOADING(state) {
            state.loading++;
        },
        SERVICE_FINISHED(state) {
            state.loading--;
        }
    }
}

export default module;
