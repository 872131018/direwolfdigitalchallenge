const module = {
    state: {
        questions: [],
        currentQuestion: {},
        currentAnswer: {},
        score: 0,
    },
    mutations: {
        LOAD_QUESTIONS(state, questions) {
            questions.map(question => {
                state.questions.push(question);
            })
        },
        LOAD_QUESTION(state, question) {
            state.currentQuestion = question;
        },
        REMOVE_QUESTION(state, index) {
            state.questions.splice(index, 1);
        },
        SET_ANSWER(state, answer) {
            state.currentAnswer = answer;
        },
        ANSWER_QUESTION(state, answer) {
            state.score += answer.score;
        },
        RESET_QUIZ(state) {
            state.questions = [];
            state.currentQuestion = {};
            state.currentAnswer = {};
            state.score = 0;
        }
    }
}

export default module;
