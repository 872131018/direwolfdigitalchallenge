import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'

Vue.config.productionTip = false

Vue.use(Vuex);
import store from './vuex/root.vuex';

new Vue({
  render: h => h(App),
  store,
}).$mount('#app')
