# direwolfdigitalchallenge

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

#Web Developer Test
##Questions
###General Knowledge:
Q. Describe a RESTful service.
A RESTful (REpresentational State Transfer) service transers state from a server to a consuming service or vice versa.  Typically this is going to be row or a collection of rows from a database or a set of data to put into a database. For instance, a list of all purchases a customer has made.  Query string parameters are often used to send additional information to the service.  For instance, filtering by date on a collection of purchases might include <code>?start_date=1-1-2018&end_date=1-1-2019</code>. This is transfered in a common format such as JSON so as to be system agnostic. Different types of HTTP requests can be used to describe the type of action to be taken.  GET and POST are two examples of types of HTTP requests that respectively GET some data or POST new data to a server.

Q. Describe MVC and MVVM.
MVC (Model View Controller) is a paradigm for creating a framework or service that accepts HTTP requests.  The controller typically receives the request and processes the request such as validating input, ensuring a user is authorized or starting a user session. The View is generally a template engine of some kind that accepts the controllers output and generates some HTML to be returned to the requester.  Things such as the current user's name or their account information are typical things that are injecting into the View layer. The Model is a general reference to a way of representing information in a data store as a class. For instance a table with rows user_id, user_name, and user_type would generate a model that in pseudocode might look like 
<code>
  class User extends Model {
    private user_id
    public user_name
    public user_type
  }

  public function getUsername() {
    return this.user_name
  }
</code>

MVVM (Model View Viewmodel) is a paradigm for creating web applications that seperates the logic of rendering HTML from performing data manipulation and transformation.  The Model has data that is typically populated from some kind of HTTP request or user input and is represented as a class or an component similar to the Model layer discussed above. The Viewmodel layer is also similar to the above discussed model and usually transforms some kind of template HTML into a final DOM to be rendered by the web browser. This is done by combining the static HTML and its directives with the Model layer to render a final DOM in the web browser. This is most easily described with a demonstration
<code>
  <p>Welcome back {{ this.user_name }}!</p>
</code>

would output
<code>
  <p>Welcome back John!</p>
</code>

Q. Describe an SPA application.
SPA (Single Page Application) is a paradigm for creating web applications which render their own HTML based on input.  They rely heavily on javascript to perform DOM manipulations and data processing in the browser and not on an external service.  A single page application can still request external services for information through an API, but the application will react to returned data internally and update its final renderings without having an external service render HTML for it.  Behavior such as the browsers backwards and forwards buttons can mimic navigating to other pages by accessing the browser API and updating its state without expressly reloading pages from an external service.

Q. What is a microservice?
A microservice is a method for laying out a large codebase as a series of small self contained applications.  What this means in practice is generally using lightweight toolkits to solve problems as a series of small pieces.  For instance, a webpage needs to display an account page that shows the users profile, recent purchases, and payment information. The page might request a user service to get the user information, a purchase service to get purchases, and a payment service to get payment details.  These services dont have to know about each other to function, they just have to return their respective piece of the page's information.  Services can also be called by each other to aggregate information.  For instance, a purchase service might make a request to a payment service to determine which form of payment was used on a particular purchase.  By seperating out the codebase into smaller pieces, they are more easily tested and maintained than a single larger application.

Q. What are the differences between two way data binding and one-way data binding? Which common JavaScript frameworks showcase them.
The main difference between two way and one way data binding is how state flows between an input and an object. In two way data binding, an update directly to the DOM flows back to the object bound to that element.  For instance, an input tag bound to an object property will always have the same value. One way data binding typically ensures the object property is the source of truth for a DOM element.  Using the input tag example, a customer may input some text but if the proper javascript doesn't execute to update the object bound to it, the input's value will not change.  One and two way data binding are both present in Vue.js and Angular 2.  React with redux primarily uses one way data binding (also known as reactive modeling in this case).
###Vue.js
Q. What is Vue.js?
Vue.js is a javascript framework for developing dynamic web appliations.  It relies on components to construct a full application out of individual pieces. These pieces render HTML from a defined template and can also apply scoped css and data from its own properties.  An advantage of using a framework such as Vue is that each component can be reused which can reduce the amount of code a complex UI needs to maintain.

Q. What are components? 
Components are the individual pieces a framework uses to construct a UI.  Each one may contain instructions on how to render some HTML, scope properties that hold data values specific to that component, or define the styles a component should know about.  Components encapsulate logic into small pieces that can be reused.

Q. What are component Props?
Props are bits of data that can be passed into a component from another component.  This data flows one way, from the parent to the child component.  This allows the seperation of components into smart components and dumb components.  An advantage of this approach is that you can consolidate your logic in a parent component and just use child components to display that data.

Q. What are the differences between ```v-bind``` and ```v-model```. Show an example.
V-bind and v-model are very similar.  The main difference is that v-model contains additional logic for capturing user input events. It is essentially shorthand for 
<code>
<input
   v-bind:value="something"
   v-on:input="something = $event.target.value"
>
</code>
This is useful because Vue comes with baked in logic for a variety of inputs including inputs, radiobuttons, and dropdowns.

Q. What is Vuex? What problems does it try to solve?
Vuex is a state storage library designed to integrate with vuex.  What that really means is that its a place for you to put your data where it can be shared across components.  This can be useful when multiple components must share the same data.  For instance when 2 components require the same data from an API, the API need only be called once.  Another advantage of this approach is that your components no longer need to manage their own state. This makes the components reactive to state change and can greatly reduce the amount of code needed to populate a component with data it requires.
###JavaScript
Q. What is the difference between ```const```, ```let```, and ```var```
Const defines a new javascript variable that is read only. This variable cannot be mutated once it has been declared.
Let defines a new javascript variable that only has scope within the function it is declared in. Meaning an attempt to access a let scoped variable outside of its function will result in a undefined error.
Var defines a new javascript variable with scope within its block. In a broswer this generally means attaching a variable to the window and making it globally available. 
Q. Explain the ```async``` and ```await``` keywords.
Async is a new syntax for defining a function as being asynchronus. Asynchronus functions may continue execution past a line of code before that code has finished executing.  Async is generally used with await, which allows execution to treat an asyncronus function synchonusly.  Interally these are using javascript Promises which is a new construct to allieviate callback chaining.  The main use of a promise is to give a better API around functions.  For instance, defining a async function allows it to call another async function with await, and to continue executing once the awaited function has resolved.

Q. What is the output of the following, and explain the differences.
```javascript
console.log(false == 0);
console.log(false === 0);
```
The main difference is that == only evaluates value and === evaluates value and type.  For instance, 0 == false evaluates to true and 0 === false does not evaluate to true.
Q. What is a closure?
A javascript closure gives an inner function access to its wrapping functions scope.  This is generally done to encapsulate function values and protect them from access outside a desired scope. A closure is declared by returning a function inside a function.  Importantly, closures are how javascript provides private variables by reducing the scope of a declared variable only to a function that is accessed by itself.

Q. Given the following
```javascript
function wait(label) {
  return new Promise(resolve => {
    setTimeout(resolve(label), 1000);
  });
}

async function f(){
  const list = [wait(“First”), wait(“Second”)];
  console.log(“Started function”);
  list.foreach(async function(x){
    console.log(“Started”);
    console.log(await x);
    console.log(“Finished”);
  });
  console.log(“Finished function”);
}
````
We want the output to look like

1. Started function
2. Started
3. First
4. Finished
5. Started
6. Second
7. Finished
8. Finished Function

What is the output and how would you fix the function to operate in the assumed manner?
Initial output is 
```
console
'Started function'
'Started'
'Started'
'Finished function'
'First'
'Finished'
'Second'
'Finished'
```
In order to fix you need to use Promise.all and reorder the console logging
```javascript
function wait(label) {
  return new Promise(resolve => {
    setTimeout(resolve(label), 1000);
  });
}

async function f(){
  const list = [wait('First'), wait('Second')];
  console.log('Started function');
  await Promise.all(list.map(async x => {
    let response = await x
    console.log('Started');
    console.log(response);
    console.log('Finished');
  }));
  console.log('Finished function');
}
```
Using Promise.all allows executing to wait for all promises given to all to resolve before it resolves.  Also note the use of map() which returns a promise instead of forEach which simply returns
##Quiz video game
###Description
- You have to make a quiz web app using the json data provided.
###Requirements
- The player can see one question at the time (done)
- The player can see a score at the end (done)
- The player should not see the same order of questions next time the quiz is taken (done)
- You cannot edit the json file (done)
- The game should be an Single Page Application (done)
- The game should be responsive and can be played on mobile devices (done)
- Use the assets provided or make your own or use CSS
- Use a Google Web Font (https://www.google.com/fonts) of your choice (done)
- You can use Bootstrap, MaterializeCSS or no framework as desired
###Extra Credit
- Add an intro screen with a PLAY button
- And a PLAY AGAIN button if the player has already played the game 
- Add animations for correct and incorrect answers by the player using CSS3
- Add play again functionality (done)
Example Quiz Layout
![Image of DWDMock](dwd_quiz.jpg)

## Instructions to run
Project is scaffolded with the vue-cli toolkit.  It does not include a production mode and runs in development mode off the webpack dev server.  In order to run just execute npm install to load dependencies and then run `npm run serve` in the root directory of the project.  It does require that node and npm be installed on your machine.


